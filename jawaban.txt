C:\xampp\mysql>cd bin

1. membuat database
create database myshop;

2.membuat table
user
 create table users(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> paswword varchar(255),
    -> primary key(id)
    -> );
items
MariaDB [myshop]> create table items(
    -> id int auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(8),
    -> stock int(8),
    -> category_id int,
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );

categories
MariaDB [myshop]> create table categories(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> );
3. Memasukkan data pada table
users
MariaDB [myshop]> insert into users(name,email,password) values ("John Doe", "jo
hn@doe.com", "john123"),("Jane Doe", "jane@doe.com", "jenita123");

categories
MariaDB [myshop]> insert into categories(name) values("gadget"),("cloth"),("men"
),("women"),("branded");

items
MariaDB [myshop]> insert into items(name, description, price, stock, category_id
) values("samsung b50", "hape keren dari merek samsung", 4000000, 10, 1),("Unikl
ooh", "baju keren dari brand ternama", 500000, 50, 2),("IMHO Watch", "jam tangan
 anak yang jujur banget", 2000000, 10, 1);

4. Mengambil data dari database
a. mengambil data users
MariaDB [myshop]> select id, name, email from users;

b. mengambil data items
- MariaDB [myshop]> select * from items where price > 1000000;
-MariaDB [myshop]> select * from items where name like '%Watch'
    -> ;

c.menampilkan data items join dengan kategori
MariaDB [myshop]> select items.id, items.name, items.description, items.price, i
tems.stock, items.category_id, categories.name from items inner join categories
on items.category_id=category_id;

5. Mengubah data dari database
MariaDB [myshop]> update items set price = 2500000 where id = 1;

